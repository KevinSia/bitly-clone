get '/' do
  @url = ''
  erb :"static/index"
end

get '/about' do
  erb :"static/about"
end

get '/table' do
  @urls = Url.all
  erb :"static/table"
end


post '/urls' do
  @url = Url.new(long_url: params[:long_url])
  @url.shorten
  if @url.save
    @total_count = Url.where(long_url: params[:long_url]).sum(:counter)
    erb :"static/index"
  else
    erb :"static/error", locals: { error: "An error occured" }
  end
end

get '/:short_url' do
  byebug
  url = Url.find_by(short_url: params[:short_url])
  url.counter += 1
  url.save
  redirect "http://" + url.long_url
end