class Url < ActiveRecord::Base
  validates :long_url, presence: true
  validates :short_url, presence: true

  # before_create :shorten
	def shorten
    self.short_url = ([*('A'..'Z'), *('0'..'9'), *('a'..'z')]-%w(0 1 I O)).sample(8).join
  end
end
