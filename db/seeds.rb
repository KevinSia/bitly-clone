# mass insert options
# Options 1: Use transaction on Model.create
#  ActiveRecord::Base.transaction do
#    1000.times { Model.create(options) }
#  end
# Options 2: Use transaction with executing many raw sql
# # Slow
# 1000.times {|i| Foo.create(:counter => i) }
#
# # with raw SQL -> FAST
# Foo.transaction do
#   1000.times do |i|
#     Foo.connection.execute "INSERT INTO foos (counter) values (#{i})"
#   end
# end
#
