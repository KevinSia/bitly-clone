class CreateUrls < ActiveRecord::Migration
	def change
    create_table :urls do |u|
      u.string :long_url, null: false
      u.string :short_url, null: false
      u.timestamps null: false
    end
	end
end
