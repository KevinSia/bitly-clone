class Person
  attr_accessor :name, :age, :id
  def initialize(name, age, id = nil)
    @name = name
    @age = age
    @id = id
  end

  def random_id
    self.id = rand(1..6).to_s + self.name
  end

  def self.show_self
    self
  end

  def show_self
    self
  end
end

p = Person.new("kevin", 18)
puts p.show_self # line 17

p2 = Person.new("beh", 19)
puts p2.show_self # line 17

puts Person.show_self # line 13
