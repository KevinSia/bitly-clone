$(document).ready(function(){
	$('#nav_form').on("submit", function(event){
		// override default form submit to use ajax submission
		event.preventDefault();

		//execute ajax request
		$.ajax({
			url: '/urls',
			method: 'post',
			data: $(this).serialize(),
			dataType: 'json',
			success: function(data){
				//get request schema and host from controller
			},
			error: function(){

			},
		});
	});
});